const Cart = require("../models/Cart.js");

// //VIEW CART
module.exports.viewCart = ( userData ) =>{
		return Cart.find({ userId: userData }, { _id:0, __v:0 }).then(res => {
			console.log(res)
				return res;
		});
};

module.exports.removeFromCart = (userId, productId) => {
	console.log(userId)
	console.log(productId)
	return Cart.findOneAndUpdate({ userId: userId},
		{ $pull : { products: {productId: productId} }})
}