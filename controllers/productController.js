const Order = require("../models/Order.js");
const User = require("../models/User.js");
const Cart = require("../models/Cart.js");
const Product = require("../models/Product.js");

//ADD PRODUCT (ADMIN)
module.exports.addProduct = ( reqBody ) => {
		return Product.find({ name: reqBody.name }).then( res => {
				if ( res.length > 0 ){
						return false;
				} else {
						let newProduct = new Product(
        	{			name: reqBody.name,
								img: reqBody.img,
          			description: reqBody.description,
          			price: reqBody.price,
          			quantity: reqBody.quantity,
          			category: reqBody.category
        	});
      			return newProduct.save().then(( product, error ) => {
        				if ( error ) {
         						return false;
        				} else {
          					return true;
        				};
      			});
   			}
  	});
};

//VIEW ALL PRODUCTS
module.exports.viewAllProducts = () =>{
	return Product.find({  }).then(result =>{
		return result;
	})
};

//VIEW ALL PRODUCTS (ACTIVE)
module.exports.viewAllActive = () =>{
	return Product.find({ isActive: true }).then(result =>{
		return result;
	})
};

//VIEW SINGLE PRODUCT
module.exports.getProduct = (req) =>{
		return Product.findById(req).then(res =>{
				return res;
		})
};

//UPDATE PRODUCT (ADMIN)
module.exports.updateProduct = ( reqBody ) => {
		let updateProduct = { 
			name: reqBody.name,
			description: reqBody.description,
			img: reqBody.img,
			price: reqBody.price,
			quantity: reqBody.quantity,
			category: reqBody.category
		}
		console.log(updateProduct)
		return Product.findByIdAndUpdate( reqBody.id, updateProduct ).then(( res, error )=>{
				if ( error ) {
						return false;
				} else {
						return true;
				}
		})
};

//REMOVE PRODUCT (ADMIN)
module.exports.removeProduct = ( id, isActive ) => {
		let active = { isActive : true };
		let inactive = { isActive : false };
		if ( isActive === true) {
			return Product.findByIdAndUpdate( id, inactive ).then(( res, error ) => {
				if ( error ) {
						return false;
				} else {
						return true;
				}
			});
		} else {
			return Product.findByIdAndUpdate( id, active ).then(( res, error ) => {
				if ( error ) {
						return false;
				} else {
						return true;
				}
			});
		};
}
				

//VIEW ALL PRODUCTS (ADMIN)
module.exports.getAllProducts = () =>{
		return Product.find({}).then(res =>{
				return res;
		})
};

