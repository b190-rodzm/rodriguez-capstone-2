const auth = require("../auth.js");
const bcrypt = require("bcrypt");

const Order = require("../models/Order.js");
const User = require("../models/User.js");
const Cart = require("../models/Cart.js");
const Product = require("../models/Product.js");

//CHECK EMAIL EXISTS
module.exports.checkEmailExists = ( reqBody ) => {
		return User.find({ email: reqBody.email }).then(res => {
				if (res.length > 0){
						return true;
				} else {
						return false;
				}
		})
};

//USER REGISTRATION
module.exports.registerUser = (reqBody) => {
	let newUser = new User(
	{		email: reqBody.email,
			password: bcrypt.hashSync( reqBody.password,10 ),
			firstName: reqBody.firstName,
			lastName: reqBody.lastName
	});
	return newUser.save().then(( user, error ) =>{
			if ( error ) {
					return false;
			} else {
					let newCart =new Cart(
					{		email: reqBody.email,
							userId: user._id
					}
					)
					newCart.save()
					return true;
			}
	})
}

//GET USER DETAILS
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {

		result.password = "";

		return result;
	});
};

//UPDATE USER DETAILS
module.exports.updateProfile = (id, data) => {
	return User.findByIdAndUpdate(id, {
		firstName: data.firstName,
		lastName: data.lastName,
		email: data.email
	})
	.then((res, error) => {
		if (error) {
			return false;
	} else {
			return true;
	}
	});
};


//USER LOGIN
module.exports.loginUser = ( reqBody ) => {
	return User.findOne( { email: reqBody.email } ).then(result =>{
		if(result === null){
			return false;
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}else{
				return false;
			}
		}
	} )
} 

//CHANGE PASSWORD
module.exports.changePassword = (userData, passwords) => {

	return User.findById(userData.id)
	.then(result =>{
		if(result === null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(passwords.oldPassword, result.password);

			if(isPasswordCorrect){
					console.log(passwords.newPassword)
				return User.findByIdAndUpdate(userData.id,
					{password: bcrypt.hashSync( passwords.newPassword,10 )}
					).then((res, error) => {
						if (error) {
							return false;
					} else {
							console.log("password changed")
							return true;
					}
					});
			}else{
				return false;
			}
		}
	})
}

//GET ALL USERS
module.exports.getAllUsers = () => {
		return User.find({}).then(res=>{
    		return res;
  	});
};

//SET USER AS ADMIN
module.exports.setAdmin = (id, isAdmin) => {
		let setAsAdmin = { isAdmin : true };
		let setAsNonAdmin = { isAdmin : false };
		if (isAdmin === true) {
			return User.findByIdAndUpdate(id, setAsNonAdmin).then((res, error) => {
				if (error) {
			  		return false;
	  		} else {
		    		return true;
	  		}
		});
		} else {
			return User.findByIdAndUpdate(id, setAsAdmin).then((res, error) => {
				if (error) {
			  		return false;
	  		} else {
		    		return true;
	  		}
		});
		}
};

//VIEW AUTH USER ORDERS
module.exports.viewUserOrders = (userData) => {
		return Order.find({userId: userData.userId}).sort({"purchasedOn": 1}).then(res => {
    		if (res.length === 0) {
        		return false;
    		} else {
       			return res
    		}
  	})
};

//VIEW ALL ORDERS (ADMIN)
module.exports.viewOrders = () => {
		return Order.find({}).then(res => {
       return res
    })
};

//CREATE ORDER / CHECKOUT
module.exports.checkout = async (userData) => {
		const userProduct = await Cart.findOne({userId: userData.userId}).select({'products': 1, '_id': 0});
		let newOrder = new Order(
        {		userId: userData.userId,
            email: userData.email
        });
    const savedOrder = await newOrder.save();

    await Order.updateOne(
      {_id: savedOrder.id},
      {$push: {products : {$each: userProduct.products}}}
		);
			// await Order.findOne({_id: savedOrder._id})
			// .select({'products.subTotal': 1, '_id': 0});
		await Order.aggregate([
						{ $match: {_id: savedOrder._id} },
						{ $addFields: { totalAmount: { $sum: "$products.subTotal"}}},
						{ $merge: { into: "orders" }}
		]);

		// await User.findByIdAndUpdate(userData.userId,
		// 				{$push: {orders : {orderId: savedOrder._id, status: savedOrder.status}}}
		// )

    return Cart.findOneAndDelete({userId: userData.userId}).then((res, error) => {
        if (error) {
            return false
        } else {
            		let newCart = new Cart({ userId: userData.userId, email: userData.email })
            		newCart.save()

								let data =[savedOrder.id, true]
            		return data;
          	}
    })
}





// ADD TO CART
module.exports.addToCart = async (data, product) => {
	const purchasedItem = await Product.findById(product.id);
	console.log(purchasedItem)
	const userCart = await Cart.find({ userId: data.userId, 'products.productId': product.id });
	if ( userCart.length === 0 ) {
		await Cart.findOneAndUpdate({ userId: data.userId },
			{ $push: { 'products': { 
											productId: product.id, 
											name: purchasedItem.name,
											img: purchasedItem.img,
											price: purchasedItem.price, 
											quantity: product.qty,
											subTotal: parseInt((product.qty*purchasedItem.price).toFixed(2))
														}		
				}})
		return true
} else if ( userCart.length === 1 ) {
		await Cart.findOneAndUpdate({ userId: data.userId, 'products.productId': product.id },
		{ $inc : {'products.$.quantity' : product.qty, 'products.$.subTotal' : purchasedItem.price*product.qty }})
		return true;
} else {
	return false
}
}



