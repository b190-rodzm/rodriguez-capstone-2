const Order = require("../models/Order.js");
const User = require("../models/User.js");
const Cart = require("../models/Cart.js");
const Products = require("../models/Product.js");

//UPDATE ORDER STATUS
module.exports.updateOrder = (reqParams, newStatus) => {
		let update = {status: newStatus};
		return Order.findByIdAndUpdate(reqParams, update).then((res, error) => {
				if (error) {
			    	return false;
	  		} else {
		    		return true;
	  		}
	});
};

module.exports.getOrder = (req) => {
	console.log(req)
		return Order.findById(req).then((res, error) => {
				if (error) {
			    	return false;
	  		} else {
		    		return res;
	  		}
	});
};

module.exports.viewOrder = (req) => {
	console.log(req)
		return Order.findById(req).then((res, error) => {
				if (error) {
			    	return false;
	  		} else {
		    		return res;
	  		}
	});
};

