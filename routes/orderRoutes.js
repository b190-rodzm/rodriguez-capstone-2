const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require("../auth.js");

module.exports = router;

//UPDATE ORDER STATUS
router.put("/:id/update", auth.verify,(req,res) => {
	let status = req.body.status
  if (auth.decode(req.headers.authorization).isAdmin) {
    orderController.updateOrder(req.params.id, status)
    .then(resultFromController => res.send (resultFromController));
  } else {
    res.status(401).send({Error: "Unauthorized request."})
  }
});


router.get("/admin/:id", (req, res) => {
  orderController.viewOrder(req.params.id)
  .then(resultFromController => res.send (resultFromController));
});

router.get("/:id", auth.verify, (req, res) => {
  orderController.getOrder(req.params.id)
  .then(resultFromController => res.send (resultFromController));
});



