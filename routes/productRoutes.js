const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require("../auth.js");

//ADD PRODUCT (ADMIN)
router.post( '/', auth.verify, ( req,res ) => { 
  if ( auth.decode ( req.headers.authorization ).isAdmin ) {
  productController.addProduct( req.body )
  .then( resultFromController => res.send( resultFromController ));
} else {
  res.status(401).send({ Error: 'Unauthorized request.' })
}
});

//VIEW ALL PRODUCTS
router.get('/all', (req, res)=>{
	productController.viewAllProducts().then(resultFromController => res.send(resultFromController));
});

//VIEW ALL PRODUCTS (ACTIVE)
router.get('/', (req, res) =>{
	productController.viewAllActive().then(resultFromController => res.send(resultFromController));
} );

//VIEW SINGLE PRODUCT
router.get( '/:id', ( req, res ) => {
  productController.getProduct( req.params.id )
  .then( resultFromController => res.send( resultFromController ));
});


//UPDATE PRODUCT (ADMIN)
router.put( '/', auth.verify, ( req, res ) => { 
  if ( auth.decode( req.headers.authorization ).isAdmin ) {
  productController.updateProduct( req.body )
  .then( resultFromController => res.send( resultFromController ));
} else {
  return false
}
});

//REMOVE PRODUCT (ADMIN)
router.put( '/status', auth.verify, ( req, res ) =>{
  if ( auth.decode( req.headers.authorization ).isAdmin ) {
    productController.removeProduct( req.body.id, req.body.isActive )
    .then( resultFromController => res.send( resultFromController ));
  } else {
    res.status(401).send(false)
  }
  });
  

//VIEW ALL PRODUCTS (ADMIN)
router.get( '/all', auth.verify, ( req, res ) =>{
if ( auth.decode( req.headers.authorization ).isAdmin ) {
  productController.getAllProducts()
  .then( resultFromController => res.send( resultFromController ));
} else {
  res.status(401).send({ Error: 'Unauthorized request.' })
}
});



module.exports = router;