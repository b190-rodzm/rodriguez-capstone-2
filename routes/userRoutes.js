const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth.js");

//CHECK EMAIL
router.post('/checkEmail', ( req, res ) =>{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
} )

//USER REGISTRATION
router.post("/register", ( req, res ) => {
	userController.registerUser( req.body ).then(resultFromController => res.send(resultFromController));
} );


//USER LOGIN
router.post("/login", ( req, res ) => {
	userController.loginUser( req.body ).then( resultFromController => res.send( resultFromController ) );
} );

//CHANGE PASSWORD
router.post("/details", ( req, res ) => {
  const userData = auth.decode(req.headers.authorization)
  const passwords = {
    oldPassword: req.body.oldPassword,
    newPassword: req.body.newPassword
  }
	userController.changePassword( userData, passwords ).then( resultFromController => res.send( resultFromController ) );
} );

//UPDATE USER DETAILS
router.put("/details", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
  const data = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email
  }
	userController.updateProfile(userId, data).then(resultFromController => res.send(resultFromController));
});

//GET USER DETAILS
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});

//GET ALL USERS
router.get("/", (req,res) => {
  userController.getAllUsers()
  .then(result => res.send (result));
});

//SET USER AS ADMIN
router.put("/setAdmin/", auth.verify,(req,res) => {
  if (auth.decode(req.headers.authorization).isAdmin) {
    userController.setAdmin(req.body.id, req.body.isAdmin)
    .then(result => res.send (result));
  } else {
    res.status(401).send({Error: "Unauthorized request."})
  }
});

//DELETE USER (ADMIN)
router.get("/", (req,res) => {
  userController.getAllUsers()
  .then(result => res.send (result));
});

//VIEW AUTH USER ORDERS
router.get("/myOrders", auth.verify, (req, res) => {
	let userData = { userId: auth.decode(req.headers.authorization).id,
    isAdmin: auth.decode(req.headers.authorization).isAdmin};
  userController.viewUserOrders(userData).then(result => res.send(result));
});

//VIEW ALL ORDERS (ADMIN)
router.post("/orders", auth.verify, (req, res) => {
  if (auth.decode(req.headers.authorization).isAdmin) {
	  userController.viewOrders().then(result => res.send(result));
  } else {
  res.status(401).send({ Error: "Unauthorized request." })
  }
});

//CREATE ORDER
router.post( "/checkout", auth.verify, (req, res) => {
	let userData = { 
		userId: auth.decode(req.headers.authorization).id,
		email: auth.decode(req.headers.authorization).email
	}
	userController.checkout(userData)
	.then(result => res.send(result));
});


//ADD TO CART
router.post( '/addToCart', auth.verify, ( req, res ) => {
	let data = { userId: auth.decode( req.headers.authorization ).id }
  let product = { id: req.body.id, qty: req.body.qty }
	userController.addToCart( data, product )
  .then( resultFromController => res.send( resultFromController ));
});



module.exports = router;