const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cartController');
const auth = require("../auth.js");

//VIEW CART
router.get( "/", auth.verify, ( req, res ) => {
	let userData = auth.decode(req.headers.authorization ).id
	cartController.viewCart( userData )
	.then( resultFromController => res.send( resultFromController ));
});


router.post( "/remove", auth.verify, ( req, res ) => {
	let userId = auth.decode(req.headers.authorization ).id

	cartController.removeFromCart( userId, req.body.productId )
	.then( resultFromController => res.send( resultFromController ));
});





// //UPDATE CART
// router.put("/cart/:id", auth.verify, (req, res) => {
// 	let userData = {
// 		userId : auth.decode(req.headers.authorization).id,
//     productId : req.body.productId,
//     quantity : req.body.quantity
// 	}
// 	cartController.updateCart(req.params, userData).then(resultFromController => res.send(resultFromController));
// });

// //DELETE CART
// router.put("/emptyCart", (req, res) => {
// 	cartController.emptyCart(data).then(resultFromController => res.send(resultFromController));
// });





module.exports = router;
