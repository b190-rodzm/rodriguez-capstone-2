const jwt = require ('jsonwebtoken');

const secret = 'ea68756d7dbe9b490cf1a39b1cfdb7f2497a6a99c0e37d52225993066534c49ffabf9d';

//CREATE ACCESS TOKEN
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	return jwt.sign(data, secret, {});
};

// TOKEN VERIFY
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if (typeof token !== "undefined") {
		// console.log(token);
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return res.status(401).send({Error: "Authentication failed."});
			} else {
				next();
			}
		})
	} else {
		return res.status(401).send({Error: "Authentication failed."});
	}
};

// TOKEN DECODE
module.exports.decode = (token) => {
	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null;
			} else {
				return jwt.decode(token, { complete : true }).payload;
			}
		})
	} else {
		return null;
	};
};
