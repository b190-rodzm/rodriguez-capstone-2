const mongoose = require('mongoose');


const cartSchema = new mongoose.Schema(
  {
  userId: { type: String, required: true },
  email: { type: String, required: true },
  products: [{
    productId: { type: String },
    name: { type: String },
    img: { type: String },
    price: { type: Number, default: 0 }, 
    quantity: { type: Number, default: 1},
    subTotal: { type: Number, default: 0 }, 
    addedOn: { type: Date, default: new Date() },
    _id : false,
  }
  ]
},
{timestamps: true}
  )

module.exports = mongoose.model('Cart', cartSchema);