const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  email: { type: String, unique: true, required: true },
  password: { type: String, required: true },
  isAdmin: { type: Boolean, default: false },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  orders: [{
    orderId:{ type: String},
    status: {type: String, default: "pending"},
    orderedOn: { type: Date, default: new Date() },
    _id: false}
  ]
},
{ timestamps: true }
);

module.exports = mongoose.model('User', userSchema);