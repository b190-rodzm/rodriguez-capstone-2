const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  // REQUIRED FOR CAPSTONE
  name: { type: String, required: true },
  img: { type: String },
  description: { type: String, required: true },
  price: { type: Number, required: true },
  isActive: { type: Boolean, default: true },
  createdOn: { type: Date, default: new Date() },
  // ADDITIONAL
  quantity: { type: Number, default: 1},
  category: { type: String }
});

module.exports = mongoose.model('Product', productSchema);