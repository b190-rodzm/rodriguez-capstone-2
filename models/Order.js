const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
  userId: { type: String, required: true },
  email: { type: String, required: true },
  products: [{
      productId: { type: String },
      name: { type: String },
      img: { type: String },
      price: { type: Number, default: 0 }, 
      quantity: { type: Number, default: 1},
      subTotal: { type: Number, default: 0},
      _id: false
      }],
  totalAmount: { type: Number, default: 0 }, 
  status: { type: String, default: "pending" },
  purchasedOn: { type: Date, default: new Date() }
});

module.exports = mongoose.model('Order', orderSchema);
