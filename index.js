const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();

const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require('./routes/productRoutes.js');
const cartRoutes = require('./routes/cartRoutes.js');
const orderRoutes = require('./routes/orderRoutes.js');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/orders", orderRoutes);
app.use("/products", productRoutes);
app.use("/cart", cartRoutes);


mongoose.connect("mongodb+srv://mikerodz:Hydepark@wdc028-course-booking.nlohbhy.mongodb.net/b190-rodriguez-capstone-2?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the database"));

app.listen(process.env.PORT || 4000, () => {console.log(`API now online at port ${process.env.PORT || 4000}`)});



// const express = require("express");
// const mongoose = require("mongoose");
// const cors = require ("cors");

// const app = express();
// const userRoutes = require('./routes/userRoutes.js');
// const orderRoutes = require('./routes/orderRoutes.js');
// const productRoutes = require('./routes/productRoutes.js');
// const cartRoutes = require('./routes/cartRoutes.js');

// app.use(express.json());
// app.use(express.urlencoded({extended: true}));
// app.use("/users", userRoutes);
// app.use("/orders", orderRoutes);
// app.use("/products", productRoutes);
// app.use("/cart", cartRoutes);
// app.use(cors());

// //MONGODB CONNECTION
// mongoose.connect("mongodb+srv://mikerodz:Hydepark@wdc028-course-booking.nlohbhy.mongodb.net/b190-rodriguez-capstone-2?retryWrites=true&w=majority",
// {useNewUrlParser : true, useUnifiedTopology : true}
// );

// let db = mongoose.connection;
// db.on("error", console.error.bind(console, "Connection error"));
// db.once("open", ()=> console.log("Connected to database"));

// //PORT
// app.listen(process.env.PORT || 4000, () => console.log(`Server online at port ${process.env.PORT || 4000}`));